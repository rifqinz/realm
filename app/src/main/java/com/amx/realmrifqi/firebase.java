package com.amx.realmrifqi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class firebase extends AppCompatActivity implements View.OnClickListener {
    Button btn, read;
    TextView tv,tvread;
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firebase);

        btn = findViewById(R.id.ambil);
        read= findViewById(R.id.read);
        tv = findViewById(R.id.textView);
        tvread= findViewById(R.id.tvread);
        btn.setOnClickListener(this);
        read.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
    if (v==btn){
        DatabaseReference myRef = database.getReference("message");

        String push = tv.getText().toString();

        myRef.setValue(push);
    }
    else if (v==read){
        DatabaseReference myRef = database.getReference("message");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                tvread.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                //Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
